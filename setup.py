#! /usr/bin/python
# -*- coding:Utf-8 -*-

from setuptools import setup
from arxivfeedlib.constants import APP_NAME

import os
import sys

with open('README.rst', encoding='utf-8') as f:
    long_description = f.read()


with open("arxivfeedlib/__init__.py") as file:
    exec(file.read())

if 'linux' in sys.platform:
    images = [os.path.join("arxivfeedlib/images/", img) for img in os.listdir("arxivfeedlib/images/")]
    data_files = [("/usr/share/applications", ["{}.desktop".format(APP_NAME)]),
                  ("/usr/share/{}/images/".format(APP_NAME), images),
                  ("/usr/share/doc/{}/".format(APP_NAME), ["README.rst", "changelog"]),
                  ("/usr/share/man/man1", ["{}.1.gz".format(APP_NAME)]),
                  ("/usr/share/pixmaps", ["{}.svg".format(APP_NAME), "{}-tray.svg".format(APP_NAME)])]
    for loc in os.listdir('arxivfeedlib/locale'):
        data_files.append(("/usr/share/locale/{}/LC_MESSAGES/".format(loc),
                           ["arxivfeedlib/locale/{}/LC_MESSAGES/{}.mo".format(loc, APP_NAME)]))
    package_data = {'arxivfeedlib.trayicon': ['packages.tcl']}
else:
    print('WARNING: this application was designed for Linux and probably will not work correctly on a different platform, especially one not using X11.')
    data_files = []
    data = ['images/*']
    for loc in os.listdir('arxivfeedlib/locale'):
        data.append("arxivfeedlib/locale/{}/LC_MESSAGES/{}.mo".format(loc, APP_NAME))
    package_data = {'arxivfeedlib': data, 'arxivfeedlib.trayicon': ['packages.tcl']}


setup(name=APP_NAME,
      version=__version__,
      description="arXiv feed viewer",
      author="Juliette Monsel",
      author_email="j_4321@protonmail.com",
      license="GPLv3",
      url="https://gitlab.com/j_4321/{}/".format(APP_NAME),
      packages=['arxivfeedlib', 'arxivfeedlib.trayicon', 'arxivfeedlib.widgets', 'arxivfeedlib.settings'],
      scripts=[APP_NAME],
      data_files=data_files,
      package_data=package_data,
      classifiers=[
              'Development Status :: 4 - Beta',
              'Intended Audience :: End Users/Desktop',
              'Environment :: X11 Applications',
              'Topic :: Internet',
              'Operating System :: POSIX :: Linux',
              'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
              'Programming Language :: Python :: 3',
              'Programming Language :: Python :: 3.5',
              'Programming Language :: Python :: 3.6',
              'Natural Language :: English',
              'Natural Language :: French'
      ],
      long_description=long_description,
      install_requires=['ewmh', 'feedparser', 'matplotlib', 'pillow', 'latexcodec'])
