#! /usr/bin/python3
# -*- coding: utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018-2019 Juliette Monsel <j_4321@protonmail.com>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Main window
"""
import logging
import traceback
import pickle
import os
import tkinter as tk
from tkinter import ttk

from PIL import Image
from PIL.ImageTk import PhotoImage

from . import constants as cst
from .constants import CONFIG, TrayIcon, SubMenu, FEEDS, CATEGORIES
from .about import About
from .settings import Config
from .messagebox import showerror, showinfo
from .version_check import UpdateChecker
from .widgets import FeedWidget, FavWidget, CatWidget


class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self, className=cst.APP_NAME)
        self.withdraw()
        logging.info('Starting {}'.format(cst.APP_NAME))

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        self.protocol('WM_DELETE_WINDOW', self.quit)

        self._icon = PhotoImage(master=self, file=cst.IM_ICON_48)
        self.iconphoto(True, self._icon)

        # --- style
        self.style = ttk.Style(self)
        self.style.theme_use('clam')
        self.style.configure("TScale", sliderlength=20)
        self.style.map("TCombobox",
                       fieldbackground=[('readonly', 'white')],
                       selectbackground=[('readonly', 'white')],
                       selectforeground=[('readonly', 'black')])
        self.style.configure("prev.TLabel", background="white")
        self.style.map("prev.TLabel", background=[("active", "white")])
        bg = self.style.lookup('TFrame', 'background', default='#ececec')
        self.configure(bg=bg)
        self.option_add('*Toplevel.background', bg)
        self.option_add('*Menu.background', bg)
        self.option_add('*Menu.tearOff', False)
        # --- --- create widgets scrollbar theme
        self._im_trough = tk.PhotoImage(name='trough-scrollbar-vert',
                                        width=15, height=15, master=self)
        bg = CONFIG.get("Widget", 'background', fallback='gray10')
        widget_bg = (0, 0, 0)
        widget_fg = (255, 255, 255)
        vmax = self.winfo_rgb('white')[0]
        color = tuple(int(val / vmax * 255) for val in widget_bg)
        active_bg = cst.active_color(color)
        active_bg2 = cst.active_color(cst.active_color(color, 'RGB'))
        slider_vert_insens = Image.new('RGBA', (13, 28), widget_bg)
        slider_vert = Image.new('RGBA', (13, 28), active_bg)
        slider_vert_active = Image.new('RGBA', (13, 28), widget_fg)
        slider_vert_prelight = Image.new('RGBA', (13, 28), active_bg2)
        self._im_trough.put(" ".join(["{" + " ".join([bg] * 15) + "}"] * 15))
        self._im_slider_vert_active = PhotoImage(slider_vert_active,
                                                 name='slider-vert-active',
                                                 master=self)
        self._im_slider_vert = PhotoImage(slider_vert,
                                          name='slider-vert',
                                          master=self)
        self._im_slider_vert_prelight = PhotoImage(slider_vert_prelight,
                                                   name='slider-vert-prelight',
                                                   master=self)
        self._im_slider_vert_insens = PhotoImage(slider_vert_insens,
                                                 name='slider-vert-insens',
                                                 master=self)
        self.style.element_create('widget.Vertical.Scrollbar.trough', 'image',
                                  'trough-scrollbar-vert')
        self.style.element_create('widget.Vertical.Scrollbar.thumb', 'image',
                                  'slider-vert',
                                  ('pressed', '!disabled', 'slider-vert-active'),
                                  ('active', '!disabled', 'slider-vert-prelight'),
                                  ('disabled', 'slider-vert-insens'), border=6,
                                  sticky='ns')
        self.style.layout('widget.Vertical.TScrollbar',
                          [('widget.Vertical.Scrollbar.trough',
                            {'children': [('widget.Vertical.Scrollbar.thumb', {'expand': '1'})],
                             'sticky': 'ns'})])
        # --- --- create fav checkbutton theme
        self._im_fav_on = PhotoImage(file=cst.IM_FAV, master=self)
        self._im_fav_on_active = PhotoImage(file=cst.IM_FAV_SEL, master=self)
        fav_off = Image.new('RGBA', (12, 12), widget_fg)
        fav_off_active = Image.new('RGBA', (12, 12), active_bg2)
        self._im_fav_off = PhotoImage(fav_off, master=self)
        self._im_fav_off_active = PhotoImage(fav_off_active, master=self)
        self.style.element_create("fav", "image", self._im_fav_off,
                                  ("!hover", "selected", "!disabled", self._im_fav_on),
                                  ("hover", "!selected", "!disabled", self._im_fav_off_active),
                                  ("hover", "selected", "!disabled", self._im_fav_on_active),
                                  border=2, sticky='')
        self.style.layout('Fav',
                          [('Fav.border',
                            {'children': [('Fav.padding',
                                           {'children': [('Fav.fav',
                                                          {'sticky': 'nswe'})],
                                            'sticky': 'nswe'})],
                             'sticky': 'nswe'})])
        self.style.map('Fav', background=[])
        # --- --- create widget hide button theme
        hide = Image.new('RGBA', (12, 12), active_bg2)
        hide_active = Image.new('RGBA', (12, 12), widget_fg)
        hide_pressed = Image.new('RGBA', (12, 12), (150, 0, 0))
        self._im_hide = PhotoImage(hide, master=self)
        self._im_hide_active = PhotoImage(hide_active, master=self)
        self._im_hide_pressed = PhotoImage(hide_pressed, master=self)
        self.style.configure('widget.hide.TButton', background=bg,
                             relief='flat', image=self._im_hide, padding=0)
        self.style.map('widget.hide.TButton', background=[], relief=[],
                       image=[('active', '!pressed', self._im_hide_active),
                              ('active', 'pressed', self._im_hide_pressed)])
        # --- --- create toggleframe theme
        closebtn = Image.new('RGBA', (12, 12), active_bg2)
        closebtn_active = Image.new('RGBA', (12, 12), widget_fg)
        closebtn_pressed = Image.new('RGBA', (12, 12), (150, 0, 0))
        toggle_open = Image.new('RGBA', (9, 9), widget_fg)
        toggle_open_active = Image.new('RGBA', (9, 9), active_bg2)
        toggle_close = Image.new('RGBA', (9, 9), widget_fg)
        toggle_close_active = Image.new('RGBA', (9, 9), active_bg2)
        self._im_closebtn = PhotoImage(closebtn, master=self)
        self._im_closebtn_active = PhotoImage(closebtn_active, master=self)
        self._im_closebtn_pressed = PhotoImage(closebtn_pressed, master=self)
        self._im_open = PhotoImage(toggle_open, master=self)
        self._im_open_active = PhotoImage(toggle_open_active, master=self)
        self._im_close = PhotoImage(toggle_close, master=self)
        self._im_close_active = PhotoImage(toggle_close_active, master=self)

        self.style.configure('toggle.close.TButton', background=bg,
                             relief='flat', image=self._im_closebtn, padding=0)
        self.style.map('toggle.close.TButton', background=[], relief=[],
                       image=[('active', '!pressed', self._im_closebtn_active),
                              ('active', 'pressed', self._im_closebtn_pressed)])

        self.style.element_create("toggle", "image", self._im_close,
                                  ("!hover", "selected", "!disabled", self._im_open),
                                  ("hover", "!selected", "!disabled", self._im_close_active),
                                  ("hover", "selected", "!disabled", self._im_open_active),
                                  border=2, sticky='')
        self.style.layout('Toggle',
                          [('Toggle.border',
                            {'children': [('Toggle.padding',
                                           {'children': [('Toggle.toggle',
                                                          {'sticky': 'nswe'})],
                                            'sticky': 'nswe'})],
                             'sticky': 'nswe'})])
        self.style.map('Toggle', background=[])

        self.widget_style_init()

        # --- systray menu
        self.icon = TrayIcon(cst.ICON, fallback_icon_path=cst.IM_ICON_48)

        self.submenu = SubMenu(parent=self.icon.menu)
        self.menu_widgets = SubMenu(parent=self.submenu)
        self.menu_widgets.add_command(label=_('Hide all'), command=self.hide_all_feeds)
        self.menu_widgets.add_command(label=_('Show all'), command=self.show_all_feeds)
        self.menu_widgets.add_separator()
        self.menu_catwidgets = SubMenu(parent=self.submenu)
        self.menu_catwidgets.add_command(label=_('Hide all'), command=self.hide_all_cats)
        self.menu_catwidgets.add_command(label=_('Show all'), command=self.show_all_cats)
        self.menu_catwidgets.add_separator()
        self.submenu.add_cascade(label=_("Feeds"), menu=self.menu_widgets)
        self.submenu.add_cascade(label=_("Categories"), menu=self.menu_catwidgets)

        self.icon.menu.add_cascade(label=_("Widgets"), menu=self.submenu)
        self.icon.menu.add_command(label=_("Settings"), command=self.config)
        self.icon.menu.add_separator()
        self.icon.menu.add_command(label=_('Check for Updates'),
                                   command=lambda: UpdateChecker(self))
        self.icon.menu.add_command(label=_('About'), command=self.about)
        self.icon.menu.add_command(label=_('Quit'), command=self.quit)
        self.icon.loop(self)

        # --- load favs
        self.entries = {}
        self.favs = []
        try:
            with open(cst.PATH_FAVS, 'rb') as file:
                p = pickle.Unpickler(file)
                self.favs = p.load()
        except Exception:
            pass
        self.fav_vars = {}
        for title in self.favs:
            self.fav_vars[title] = tk.BooleanVar(self, True)
            cst.add_trace(self.fav_vars[title], 'write',
                          lambda *x, t=title: self.fav_trace(t))
        # --- load categories
        cat_list = sorted(CATEGORIES.sections())
        self.cats = {c: [] for c in cat_list}
        try:
            with open(cst.PATH_CATEGORY_DATA, 'rb') as file:
                p = pickle.Unpickler(file)
                self.cats.update(p.load())
        except Exception:
            pass
        self.cat_vars = {}
        self.prev_cat = {}
        for cat, l in self.cats.items():
            for title in l:
                self.cat_vars[title] = tk.StringVar(self, cat)
                self.prev_cat[title] = cat
                cst.add_trace(self.cat_vars[title], 'write',
                              lambda *x, t=title: self.category_trace(t))

        # --- restore widgets
        self.widgets = {}
        self.catwidgets = {}
        self.favwidget = FavWidget(self)
        self.submenu.add_checkbutton(label=_('Favorites'),
                                     command=self.toggle_favwidget)
        cst.add_trace(self.favwidget.variable, 'write',
                      lambda *args: self.favwidget_trace())
        self.favwidget.variable.set(FEEDS.getboolean('Favorites', 'visible'))

        for cat in cat_list:
            self.add_category(cat)
        for name in cst.RSS_CAT:
            if cst.FEEDS.getboolean(name, 'active'):
                self.add_widget(name)

        # --- update check
        if CONFIG.getboolean("General", "check_update"):
            UpdateChecker(self)

        self.bind_class('TCombobox', '<<ComboboxSelected>>', self._unselect_cb)

        if not self.widgets:
            showinfo(_('Information'),
                     _('Click on the system tray icon, then on "Settings" to activate the arXiv feeds you want to follow.'))

    @staticmethod
    def _unselect_cb(event):
        event.widget.selection_clear()

    def fav_trace(self, title):
        if self.fav_vars[title].get():
            summary, authors, url, date = self.entries[title]
            self.favs.append(title)
            self.favwidget.entry_add(title, summary, authors, url, date)
        else:
            self.favs.remove(title)
            self.favwidget.entry_delete(title)
        self.save_favs()
        self.favwidget.save()

    def category_trace(self, title):
        cat = self.cat_vars[title].get()
        prev_cat = self.prev_cat[title]
        if prev_cat != cat:
            if prev_cat in self.cats:
                self.cats[prev_cat].remove(title)
                self.catwidgets[prev_cat].entry_delete(title)
                self.catwidgets[prev_cat].save()
            self.prev_cat[title] = cat

            if cat:
                summary, authors, url, date = self.entries[title]
                self.cats[cat].append(title)
                self.catwidgets[cat].entry_add(title, summary, authors, url, date)
                self.catwidgets[cat].save()

            self.save_categories()

    def update_fav(self, title, summary, authors, url, date=None):
        if title in self.favs:
            self.favwidget.entry_add(title, summary, authors, url, date)
            self.favwidget.save()

    def update_category(self, title, summary, authors, url, date=None):
        cat = self.cat_vars[title].get()
        if cat:
            self.catwidgets[cat].entry_add(title, summary, authors, url, date)
            self.catwidgets[cat].save()

    def hide_all_feeds(self):
        """Withdraw all feed widgets."""
        for widget in self.widgets.values():
            widget.withdraw()

    def show_all_feeds(self):
        """Deiconify all feed widgets."""
        for widget in self.widgets.values():
            widget.deiconify()

    def hide_all_cats(self):
        """Withdraw all category widgets."""
        for cat, widget in self.catwidgets.items():
            if cat != 'All':
                widget.withdraw()

    def show_all_cats(self):
        """Deiconify all category widgets."""
        for cat, widget in self.catwidgets.items():
            if cat != 'All':
                widget.deiconify()

    def save_favs(self):
        with open(cst.PATH_FAVS, 'wb') as file:
            p = pickle.Pickler(file)
            p.dump(self.favs)

    def save_categories(self):
        with open(cst.PATH_CATEGORY_DATA, 'wb') as file:
            p = pickle.Pickler(file)
            p.dump(self.cats)

    def add_category(self, name):
        w = CatWidget(self, name)
        self.catwidgets[name] = w
        self.menu_catwidgets.add_checkbutton(label=name,
                                             command=lambda t=name: self.toggle_catwidget(t))
        cst.add_trace(w.variable, 'write',
                      lambda *args, t=name: self.catwidget_trace(t))
        w.variable.set(CATEGORIES.get(name, 'visible'))

    def add_widget(self, name):
        w = FeedWidget(self, name)
        self.widgets[name] = w
        self.menu_widgets.add_checkbutton(label=name,
                                          command=lambda t=name: self.toggle_widget(t))
        cst.add_trace(w.variable, 'write',
                      lambda *args, t=name: self.widget_trace(t))
        w.variable.set(FEEDS.get(name, 'visible'))
        w.update_feed()

    def remove_widget(self, name):
        self.widgets[name].destroy()
        self.menu_widgets.delete(self.menu_widgets.index(name))
        del self.widgets[name]

    def remove_category(self, name):
        if name in self.catwidgets:
            self.catwidgets[name].destroy()
            self.menu_catwidgets.delete(self.menu_catwidgets.index(name))
            del self.catwidgets[name]
            del self.cats[name]
            try:
                os.remove(cst.PATH_FEED_DATA.format(category=name))
            except FileNotFoundError:
                pass

    def rename_category(self, old_name, new_name, update_cat_list):
        self.cats[new_name] = self.cats[old_name]
        self.catwidgets[new_name] = self.catwidgets[old_name]
        del self.cats[old_name]
        del self.catwidgets[old_name]
        os.rename(cst.PATH_FEED_DATA.format(category=old_name),
                  cst.PATH_FEED_DATA.format(category=new_name))
        cst.create_cat(new_name, CATEGORIES.get(old_name, 'geometry'),
                       CATEGORIES.get(old_name, 'position'),
                       CATEGORIES.get(old_name, 'visible'))
        CATEGORIES.remove_section(old_name)
        cst.save_categories()
        self.menu_catwidgets.delete(old_name)
        self.menu_catwidgets.add_checkbutton(label=new_name,
                                             command=lambda t=new_name: self.toggle_catwidget(t))
        var = self.catwidgets[new_name].variable
        cst.remove_trace(var, 'write', cst.info_trace(var)[-1][-1])
        self.catwidgets[new_name].rename(new_name)

        cst.add_trace(var, 'write', lambda *args, t=new_name: self.catwidget_trace(t))

        for title in self.cats[new_name]:
            self.cat_vars[title].set(new_name)
        if update_cat_list:
            for w in self.catwidgets.values():
                w.update_cats()
            for w in self.widgets.values():
                w.update_cats()
            self.favwidget.update_cats()

    def report_callback_exception(self, *args):
        """Log exceptions."""
        err = "".join(traceback.format_exception(*args))
        logging.error(err)
        showerror(_("Error"), str(args[1]), err, True)

    def toggle_catwidget(self, name):
        value = self.menu_catwidgets.get_item_value(name)
        if value:
            self.catwidgets[name].deiconify()
        else:
            self.catwidgets[name].withdraw()
        self.update_idletasks()

    def toggle_favwidget(self):
        value = self.submenu.get_item_value(_('Favorites'))
        if value:
            self.favwidget.deiconify()
        else:
            self.favwidget.withdraw()
        self.update_idletasks()

    def toggle_widget(self, name):
        value = self.menu_widgets.get_item_value(name)
        if value:
            self.widgets[name].deiconify()
        else:
            self.widgets[name].withdraw()
        self.update_idletasks()

    def widget_trace(self, name):
        value = self.widgets[name].variable.get()
        self.menu_widgets.set_item_value(name, value)
        FEEDS.set(self.widgets[name].name, 'visible', str(value))
        cst.save_feeds()

    def favwidget_trace(self):
        name = _('Favorites')
        value = self.favwidget.variable.get()
        self.submenu.set_item_value(name, value)
        FEEDS.set(self.favwidget.name, 'visible', str(value))
        cst.save_feeds()

    def catwidget_trace(self, name):
        value = self.catwidgets[name].variable.get()
        self.menu_catwidgets.set_item_value(name, value)
        CATEGORIES.set(self.catwidgets[name].name, 'visible', str(value))
        cst.save_categories()

    def about(self):
        About(self)

    def config(self):
        c = Config(self)
        self.wait_window(c)
        cst.save_config()
        cst.save_feeds()
        self.widget_style_init()
        splash_change, name_changes, cat_change, menu_length_change = c.changes
        if splash_change:
            self.favwidget.update_position()
        if cat_change or name_changes:
            self.favwidget.update_cats()
        self.favwidget.update_style()
        if menu_length_change:
            self.favwidget.update_menu_length()
        for name in CATEGORIES.sections():
            if name not in self.catwidgets:
                self.add_category(name)
                self.cats[name] = []
            else:
                if name in name_changes:
                    old_name = name_changes[name]
                    self.rename_category(old_name, name, False)
                self.catwidgets[name].update_style()
                if cat_change or name_changes:
                    self.catwidgets[name].update_cats()
                if menu_length_change:
                    self.catwidgets[name].update_menu_length()
        for name in FEEDS.sections():
            if name != 'Favorites':
                if FEEDS.getboolean(name, 'active', fallback=True):
                    if name not in self.widgets:
                        self.add_widget(name)
                    else:
                        self.widgets[name].update_style()
                        if cat_change or name_changes:
                            self.widgets[name].update_cats()
                        if splash_change:
                            self.widgets[name].update_position()
                        if menu_length_change:
                            self.widgets[name].update_menu_length()
                else:
                    if name in self.widgets:
                        self.remove_widget(name)

    def quit(self):
        logging.info('Closing {}'.format(cst.APP_NAME))
        self.destroy()

    def widget_style_init(self):
        """Init widgets style."""
        # --- colors
        bg = CONFIG.get('Widget', 'background', fallback='gray10')
        fg = CONFIG.get('Widget', 'foreground', fallback='white')
        feed_bg = CONFIG.get('Widget', 'feed_background', fallback='gray20')
        feed_fg = CONFIG.get('Widget', 'feed_foreground', fallback='white')
        vmax = self.winfo_rgb('white')[0]
        widget_bg = tuple(int(val / vmax * 255) for val in self.winfo_rgb(bg))
        widget_fg = tuple(int(val / vmax * 255) for val in self.winfo_rgb(fg))
        active_bg = cst.active_color(widget_bg)
        active_bg2 = cst.active_color(cst.active_color(widget_bg, 'RGB'))
        # --- scrollbar
        slider_alpha = Image.open(cst.IM_SCROLL_ALPHA)
        slider_vert_insens = Image.new('RGBA', (13, 28), widget_bg)
        slider_vert = Image.new('RGBA', (13, 28), active_bg)
        slider_vert.putalpha(slider_alpha)
        slider_vert_active = Image.new('RGBA', (13, 28), widget_fg)
        slider_vert_active.putalpha(slider_alpha)
        slider_vert_prelight = Image.new('RGBA', (13, 28), active_bg2)
        slider_vert_prelight.putalpha(slider_alpha)

        self._im_slider_vert_active.paste(slider_vert_active)
        self._im_slider_vert.paste(slider_vert)
        self._im_slider_vert_prelight.paste(slider_vert_prelight)
        self._im_slider_vert_insens.paste(slider_vert_insens)
        self._im_trough.put(" ".join(["{" + " ".join([bg] * 15) + "}"] * 15))
        # --- fav
        fav_alpha = Image.open(cst.IM_FAV_ALPHA)
        fav_off = Image.new('RGBA', (12, 12), widget_fg)
        fav_off.putalpha(fav_alpha)
        fav_off_active = Image.new('RGBA', (12, 12), active_bg2)
        fav_off_active.putalpha(fav_alpha)
        self._im_fav_off.paste(fav_off)
        self._im_fav_off_active.paste(fav_off_active)
        # --- hide button
        hide_alpha = Image.open(cst.IM_HIDE_ALPHA)
        hide = Image.new('RGBA', (12, 12), active_bg)
        hide.putalpha(hide_alpha)
        hide_active = Image.new('RGBA', (12, 12), active_bg2)
        hide_active.putalpha(hide_alpha)
        hide_pressed = Image.new('RGBA', (12, 12), widget_fg)
        hide_pressed.putalpha(hide_alpha)
        self._im_hide.paste(hide)
        self._im_hide_active.paste(hide_active)
        self._im_hide_pressed.paste(hide_pressed)
        # --- toggle
        closebtn = Image.new('RGBA', (12, 12), widget_fg)
        closebtn.putalpha(hide_alpha)
        closebtn_active = Image.new('RGBA', (12, 12), active_bg2)
        closebtn_active.putalpha(hide_alpha)
        closebtn_pressed = Image.new('RGBA', (12, 12), active_bg)
        closebtn_pressed.putalpha(hide_alpha)
        toggle_open_alpha = Image.open(cst.IM_OPENED_ALPHA)
        toggle_open = Image.new('RGBA', (9, 9), widget_fg)
        toggle_open.putalpha(toggle_open_alpha)
        toggle_open_active = Image.new('RGBA', (9, 9), active_bg2)
        toggle_open_active.putalpha(toggle_open_alpha)
        toggle_close_alpha = Image.open(cst.IM_CLOSED_ALPHA)
        toggle_close = Image.new('RGBA', (9, 9), widget_fg)
        toggle_close.putalpha(toggle_close_alpha)
        toggle_close_active = Image.new('RGBA', (9, 9), active_bg2)
        toggle_close_active.putalpha(toggle_close_alpha)
        self._im_closebtn.paste(closebtn)
        self._im_closebtn_active.paste(closebtn_active)
        self._im_closebtn_pressed.paste(closebtn_pressed)
        self._im_open.paste(toggle_open)
        self._im_open_active.paste(toggle_open_active)
        self._im_close.paste(toggle_close)
        self._im_close_active.paste(toggle_close_active)
        # --- widget style
        feed_font = CONFIG.get('Widget', 'font')
        self.style.configure('Toggle', background=bg)
        self.style.configure('Fav', background=bg)
        self.style.configure('widget.TFrame', background=bg)
        self.style.configure('widget.interior.TFrame', background=feed_bg)
        self.style.configure('widget.interior.TLabel', background=feed_bg,
                             foreground=feed_fg, font=feed_font)
        self.style.configure('toggle.close.TButton', background=bg)
        self.style.configure('widget.hide.TButton', background=bg)
        self.style.configure('widget.TSizegrip', background=bg)
        self.style.configure('widget.Horizontal.TSeparator', background=bg)
        self.style.configure('widget.TLabel', background=bg,
                             foreground=fg, font=feed_font)
        self.style.configure('widget.title.TLabel', background=bg, foreground=fg,
                             font=CONFIG.get('Widget', 'font_title'))
        self.style.configure('widget.TButton', background=bg, foreground=fg,
                             padding=1, relief='flat')
        self.style.configure('widget.TCombobox', background=bg, foreground=feed_fg,
                             padding=1, bordercolor=bg, darkcolor=bg, fieldbackground=feed_bg,
                             lightcolor=bg, arrowcolor=fg)
        self.style.map('widget.TButton', background=[('disabled', active_bg),
                                                     ('pressed', fg),
                                                     ('active', active_bg)],
                       foreground=[('pressed', bg)])
        self.style.map('widget.TCombobox',
                       foreground=[('readonly', feed_fg)],
                       fieldbackground=[('readonly', feed_bg)],
                       background=[('disabled', 'readonly', active_bg),
                                   ('readonly', bg),
                                   ('pressed', 'readonly', fg),
                                   ('active', 'readonly', active_bg)],
                       bordercolor=[('disabled', 'readonly', active_bg),
                                    ('readonly', bg),
                                    ('pressed', 'readonly', fg),
                                    ('active', 'readonly', active_bg)],
                       darkcolor=[('disabled', 'readonly', active_bg),
                                  ('pressed', 'readonly', fg),
                                  ('readonly', bg),
                                  ('active', 'readonly', active_bg)],
                       lightcolor=[('disabled', 'readonly', active_bg),
                                   ('pressed', 'readonly', fg),
                                   ('readonly', bg),
                                   ('active', 'readonly', active_bg)],
                       arrowcolor=[('readonly', fg)])
        self.style.map('widget.TLabel', background=[('selected', active_bg)])

        self.update_idletasks()
