#! /usr/bin/python3
# -*- coding:Utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018 Juliette Monsel <j_4321@protonmail.com>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Feed utils: parsers and text display
"""
from tkinter import Text
from tkinter.font import Font
from webbrowser import open as webopen
from html.parser import HTMLParser
import re
from io import BytesIO

from PIL import Image, ImageTk
import matplotlib
matplotlib.use("tkagg")
from matplotlib import pyplot as plt

from .constants import CONFIG


class FeedText(Text):

    def __init__(self, master=None, **kwargs):
        kwargs.setdefault('relief', 'flat')
        kwargs.setdefault('highlightthickness', 0)
        kwargs.setdefault('wrap', 'word')
        Text.__init__(self, master, **kwargs)

        self.images = []
        self._link_color = 'blue'
        self._font_size = 10

        self.update_style()
        self.tag_configure('date', spacing3=8)

        self.bind('<Configure>', self._on_configure)
        self.bind('<1>', lambda e: self.focus_set())

    def _on_enter_link(self, event, tag):
        self.configure(cursor='hand1')
        self.tag_configure(tag, underline=True, underlinefg=self._link_color)

    def _on_leave_link(self, event, tag):
        self.configure(cursor='xterm')
        self.tag_configure(tag, underline=False)

    def _on_configure(self, event=None):
        self.update_idletasks()
        b0 = self.bbox('1.0')
        b1 = self.bbox('3.end')
        if b0:
            x0, y0, w0, h0 = b0
            if b1:
                x1, y1, w1, h1 = b1
                height = (y1 - y0) // h1 + 2
                self.configure(height=height)
            elif (w0, h0) != (1, 1):
                self.configure(height=self['height'] + 1)
                self.update_idletasks()

    def update_style(self):
        font = CONFIG.get('Widget', 'font', fallback='TkDefaultFont')
        self._font_size = Font(font=font, master=self).actual()['size']
        self._link_color = CONFIG.get('Widget', 'link_color')
        self.focus_force()
        # self.configure(state='normal')
        self.configure(bg=CONFIG.get('Widget', 'feed_background', fallback='gray20'),
                       fg=CONFIG.get('Widget', 'feed_foreground', fallback='white'),
                       font=font)
        self.update_idletasks()
        # self.configure(state='disabled')
        self.tag_configure('link', foreground=self._link_color, spacing3=8)
        for img, latex in self.images:
            pil_img = self.parse(latex)
            img.paste(pil_img)

    def parse(self, txt):
        """Return PIL.Image of latex rendering of txt."""
        try:
            buf = BytesIO()
            fig = plt.figure()
            fig.text(0, 0, txt, fontsize=self._font_size, color=self['fg'])
            fig.savefig(buf, dpi=90, transparent=True, format="png",
                        bbox_inches='tight', pad_inches=0.0)
            plt.close(fig)
            buf.seek(0)
            return Image.open(buf)

        except Exception as e:
            print(type(e), e, txt)

    def display_feed(self, authors, content, date=None):
        content = content.encode().decode('latex')
        res = list(content)
        matches = []
        self.configure(state='normal')
        self.delete('1.0', 'end')
        # --- authors
        for i, (name, href) in enumerate(authors):
            tag = 'author#{}'.format(i)
            self.insert('1.end', name, ['link', tag])
            self.insert('1.end', ', ')
            self.tag_bind(tag, '<1>', lambda e, url=href: webopen(url))
            self.tag_bind(tag, '<Enter>', lambda e, t=tag: self._on_enter_link(e, t))
            self.tag_bind(tag, '<Leave>', lambda e, t=tag: self._on_leave_link(e, t))
        self.delete('1.end-2c', '1.end')
        self.insert('1.end', '\n')
        if date is None:
            d = '\n'
        else:
            d = date.strftime('%x\n')
        self.insert('2.0', d, ['date'])
        # --- summary display (with latex rendering)
        self.images.clear()
        for match in re.finditer(r'\$[^\$]+\$', content):
            pil_img = self.parse(match.group())
            if pil_img is not None:
                matches.append(match.span())
                self.images.append((ImageTk.PhotoImage(pil_img, master=self),
                                    match.group()))
        offset = 0
        imgs = {}

        for (start, end), (img, latex) in zip(reversed(matches), reversed(self.images)):
            del res[start: end]
        for (start, end), (img, latex) in zip(matches, self.images):
            imgs[start - offset] = img
            offset += end - start - 1
        self.insert('3.0', ''.join(res))
        for index, img in imgs.items():
            self.image_create('3.{}'.format(index), image=img)
        self.configure(state='disabled')


class AuthorParser(HTMLParser):
    def __init__(self, *args, **kwargs):
        HTMLParser.__init__(self, *args, **kwargs)
        self.author = ''
        self.href = ''
        self._tag = False

    def handle_starttag(self, tag, attrs):
        if tag == "a":
            self._tag = True
            for name, value in attrs:
                if name == 'href':
                    self.href = value

    def handle_endtag(self, tag):
        self._tag = False

    def handle_data(self, data):
        if self._tag:
            self.author = data.strip()

    def feed(self, data):
        self.author = ''
        self.href = ''
        HTMLParser.feed(self, data)
        return self.author, self.href


class SummaryParser(HTMLParser):
    def __init__(self, *args, **kwargs):
        HTMLParser.__init__(self, *args, **kwargs)
        self.summary = []

    def handle_starttag(self, tag, attrs):
        pass

    def handle_endtag(self, tag):
        pass

    def handle_data(self, data):
        self.summary.extend(data.strip().splitlines())

    def feed(self, data):
        self.summary = []
        HTMLParser.feed(self, data)
        return ' '.join(self.summary)
