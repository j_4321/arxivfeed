#! /usr/bin/python3
# -*- coding: utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018 Juliette Monsel <j_4321@protonmail.com>
based on code by RedFantom Copyright (C) 2017
<https://github.com/RedFantom/ttkwidgets/blob/master/ttkwidgets/frames/toggledframe.py>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


ToggledFrame class.
"""
from tkinter.ttk import Frame, Checkbutton, Label, Button


class ToggledFrame(Frame):
    """
    A frame that can be toggled to open and close
    """

    def __init__(self, master=None, text="", closecmd=lambda: None,
                 fav_var=None, **kwargs):
        font = kwargs.pop('font', '')
        Frame.__init__(self, master, **kwargs)
        self.style_name = self.cget('style')
        self.columnconfigure(2, weight=1)
        self.rowconfigure(1, weight=1)
        self._checkbutton = Checkbutton(self,
                                        style='Toggle',
                                        command=self.toggle,
                                        cursor='arrow')
        self.fav = fav_var
        self._fav = Checkbutton(self, style='Fav', cursor='arrow', variable=self.fav)
        self.close_btn = Button(self, style='toggle.close.TButton', command=closecmd)
        self.label = Label(self, text=text, font=font,
                           style=self.style_name.replace('TFrame', 'TLabel'))
        self.interior = Frame(self, style=self.style_name)
        self._checkbutton.grid(row=0, column=1)
        self._fav.grid(row=0, column=3)
        self.label.grid(row=0, column=2, sticky="we")
        self.close_btn.grid(row=0, column=0, sticky="e", padx=(0, 8))
        self.interior.grid(row=1, column=2, columnspan=2, sticky="nswe", padx=(4, 0))
        self.interior.grid_remove()
        self.label.bind('<Configure>', self._wrap)
        self.label.bind('<1>', lambda e: self._checkbutton.invoke())

    def _wrap(self, event):
        self.label.configure(wraplength=self.label.winfo_width())

    def toggle(self):
        if 'selected' not in self._checkbutton.state():
            self.interior.grid_remove()
            self.event_generate("<<ToggledFrameClose>>")
        else:
            self.interior.grid()
            self.event_generate("<<ToggledFrameOpen>>")

    def open(self):
        self._checkbutton.state(('selected',))
        self.interior.grid()
        self.event_generate("<<ToggledFrameOpen>>")

    def close(self):
        self._checkbutton.state(('!selected',))
        self.interior.grid_remove()
        self.event_generate("<<ToggledFrameClose>>")
