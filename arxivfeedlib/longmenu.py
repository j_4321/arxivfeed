#! /usr/bin/python3
# -*- coding:Utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018 Juliette Monsel <j_4321@protonmail.com>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Long menu
"""
from tkinter import Menu, TclError

from PIL.ImageTk import PhotoImage

from arxivfeedlib.constants import CONFIG, IM_GO_BOTTOM, IM_GO_DOWN, IM_GO_TOP, IM_GO_UP


class LongMenu(Menu):
    def __init__(self, master,
                 length=CONFIG.getint('General', 'menu_length', fallback=30), **kw):
        Menu.__init__(self, master, tearoff=False, **kw)
        self._length = length
        self._entries = []
        self._index = 0
        self._im_up = PhotoImage(file=IM_GO_UP, master=self, name='up')
        self._im_down = PhotoImage(file=IM_GO_DOWN, master=self, name='down')
        self._im_top = PhotoImage(file=IM_GO_TOP, master=self, name='top')
        self._im_bottom = PhotoImage(file=IM_GO_BOTTOM, master=self, name='bottom')
        Menu.add(self, 'command', label='  ', compound='right', imag='top',
                 command=self.top, state='disabled')
        Menu.add(self, 'command', label='  ', compound='right', imag='up',
                 command=self.prev, state='disabled')
        Menu.add(self, 'command', label='  ', compound='right', imag='down',
                 command=self.next, state='disabled')
        Menu.add(self, 'command', label='  ', compound='right', imag='bottom',
                 command=self.bottom, state='disabled')

    @property
    def length(self):
        return self._length

    @length.setter
    def length(self, length):
        self._length = length
        nb_entries = self.index('end') - 3  # nb of visible entries
        while nb_entries > self._length:    # too many entries
            Menu.delete(self, nb_entries + 1)
            nb_entries -= 1
        ind = nb_entries + self._index
        while nb_entries < self._length and ind < len(self._entries):
            itemType, kw = self._entries[ind]
            self.insert(nb_entries + 2, itemType, kw)
            ind += 1
            nb_entries += 1
        while nb_entries < self._length and self._index > 0:
            itemType, kw = self._entries[self._index - 1]
            self.insert(2, itemType, kw)
            nb_entries += 1
            self._index -= 1
        if self._index == 0:
            self.entryconfigure(0, state='disabled')
            self.entryconfigure(1, state='disabled')
        if self._index + self.length >= len(self._entries):
            self.entryconfigure(self.index('end'), state='disabled')
            self.entryconfigure(self.index('end') - 1, state='disabled')

    def top(self):
        x, y = self.winfo_rootx(), self.winfo_rooty()
        for i in reversed(range(min(self._index, self.length))):
            itemType, kw = self._entries[i]
            self.insert(2, itemType, kw)
        nb_entries = self.index('end') - 3
        while nb_entries > self.length:
            Menu.delete(self, nb_entries + 1)
            nb_entries -= 1
        self.entryconfigure(self.index('end'), state='normal')
        self.entryconfigure(self.index('end') - 1, state='normal')
        self.entryconfigure(0, state='disabled')
        self.entryconfigure(1, state='disabled')
        self._index = 0
        self.tk_popup(x, y)

    def prev(self):
        x, y = self.winfo_rootx(), self.winfo_rooty()
        if self._index > 0:
            self._index -= 1
            itemType, kw = self._entries[self._index]
            self.insert(2, itemType, kw)
            nb_entries = self.index('end') - 3
            while nb_entries > self.length:
                Menu.delete(self, nb_entries + 1)
                nb_entries -= 1
            self.entryconfigure(self.index('end'), state='normal')
            self.entryconfigure(self.index('end') - 1, state='normal')
            if self._index == 0:
                self.entryconfigure(0, state='disabled')
                self.entryconfigure(1, state='disabled')
        self.tk_popup(x, y)

    def next(self):
        x, y = self.winfo_rootx(), self.winfo_rooty()
        ind = self._index + self.length
        if ind < len(self._entries):
            self._index += 1
            itemType, kw = self._entries[ind]
            nb_entries = self.index('end') - 2
            self.insert(nb_entries + 1, itemType, kw)
            while nb_entries > self.length:
                Menu.delete(self, 2)
                nb_entries -= 1
            self.entryconfigure(0, state='normal')
            self.entryconfigure(1, state='normal')
            if self._index + self.length >= len(self._entries):
                self.entryconfigure(self.index('end'), state='disabled')
                self.entryconfigure(self.index('end') - 1, state='disabled')
        self.tk_popup(x, y)

    def bottom(self):
        x, y = self.winfo_rootx(), self.winfo_rooty()
        ind = self._index + self.length
        l_entries = len(self._entries)
        for i in range(max(ind, l_entries - self.length), l_entries):
            itemType, kw = self._entries[i]
            self.insert(self.index('end') - 1, itemType, kw)
        nb_entries = self.index('end') - 3
        while nb_entries > self.length:
            Menu.delete(self, 2)
            nb_entries -= 1
        self.entryconfigure(0, state='normal')
        self.entryconfigure(1, state='normal')
        self.entryconfigure(self.index('end'), state='disabled')
        self.entryconfigure(self.index('end') - 1, state='disabled')
        self._index = l_entries - self.length
        self.tk_popup(x, y)

    def in_menu(self, label):
        i = 0
        while i < len(self._entries) and self._entries[i][1]['label'] != label:
            i += 1
        return i < len(self._entries)

    def delete(self, label):
        i = 0
        while i < len(self._entries) and self._entries[i][1]['label'] != label:
            i += 1
        if i < len(self._entries):
            ind = i + 2 - self._index
            if 1 < ind < self.index('end') - 1:
                Menu.delete(self, ind)
            if ind <= 1:
                self._index -= 1
            del self._entries[i]
        else:
            raise TclError('bad menu entry index "{}"'.format(label))
        nb_entries = self.index('end') - 3
        if nb_entries < self.length:
            ind = self._index + self.length - 1
            if ind < len(self._entries):
                itemType, kw = self._entries[ind]
                self.insert(nb_entries + 2, itemType, kw)
            elif self._index > 0:
                self._index -= 1
                itemType, kw = self._entries[self._index]
                self.insert(2, itemType, kw)
        if self._index == 0:
            self.entryconfigure(0, state='disabled')
            self.entryconfigure(1, state='disabled')
        if self._index + self.length >= len(self._entries):
            self.entryconfigure(self.index('end'), state='disabled')
            self.entryconfigure(self.index('end') - 1, state='disabled')

    def add(self, itemType, cnf, **kw):
        kwargs = {}
        kwargs.update(cnf)
        kwargs.update(kw)
        self._entries.append((itemType, kwargs))
        end = self.index('end') - 1
        if end - 1 <= self.length:
            self.insert(end, itemType, kwargs)
        else:
            self.entryconfigure(end, state='normal')
            self.entryconfigure(end + 1, state='normal')
