��    L      |  e   �      p  E   q     �     �     �     �     �     �     �     �  
                  +     I  6   g  a   �        	                    
   *  5   5     k  	   q  
   {     �     �     �  $   �     �     �  "   �     	     	     	     	     "	     +	  
   3	     >	     P	     ]	     b	  
   e	     p	     w	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
  I   
  F   P
     �
  	   �
     �
     �
     �
     �
  �   �
     F  7   X  �   �  �   w  l  e  Q   �  	   $     .  	   F     P     ^  
   n     y     ~     �  
   �     �  )   �  *   �  A        P     �     �     �     �     �       9        I     P     X     h     m     t  A   �  	   �     �  /   �               &     2     ;     B     J     \     r     �     �     �     �     �     �     �     �     �     �     �                    +  
   <     G     U     c     i  _   o  M   �       	   #     -     :     A     I  �   M     �  J   �  �   <    #        =   @   	                                          >          H   L   <              B   8       4      ,   5         (   F   3   9               "   A      )      -       1   .      7   ;   
   :   *      ?   0   6   G             J                       C   K             I   !       /   E          $         +       2            #       %   D       '         &       A new version of {app_name} is available.
Do you want to download it? About About {app_name} Above Active feeds: Background color Below Bold Cancel Categories Category Check for Updates Check for updates on start-up Check for updates on startup. Check this box if the widgets disappear when you click Click on the system tray icon, then on "Settings" to activate the arXiv feeds you want to follow. Close Close all Color Colors Confirmation Deactivate Do you really want to delete the category {category}? Error Favorites Feed entry Feeds Font Foreground color GUI Toolkit for the system tray icon General Hidden entries Hidden entries menu maximum length Hide Hide all Information Italic Language License Link color Most recent first New Category Next No No result. Normal Ok Oldest first Opacity Open Open all Please report this bug on  Position Previous Quit Sample text Save As Search Settings Show all Sort Text The GUI Toolkit setting will take effect after restarting the application The language setting will take effect after restarting the application Title Underline Update Widget Widgets Yes You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/. arXiv feed viewer {app_name} is already running, if not delete {pidfile}. {app_name} is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. {app_name} is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-04-30 13:43+0200
PO-Revision-Date: 2019-04-30 13:43+0200
Last-Translator: Juliette Monsel <j_4321@protonmail.com>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Une nouvelle version de {app_name} est disponible.
Voulez-vous la télécharger ? À propos À propos de {app_name} Au-dessus Flux actifs : Couleur du fond En-dessous Gras Annuler Catégories Catégorie Vérifier les mises à jour Vérifier les mises à jour au démarrage Vérifier les mises à jour au démarrage. Cochez cette case si les widgets disparaissent quand vous cliquez Cliquez sur l'icône dans la zone de notification, puis sur "Préférences" pour activer les flux arXiv que vous voulez suivre. Fermer Réduire tout Couleur Couleurs Confirmation Désactiver Voulez-vous vraiment supprimer la catégorie {category} ? Erreur Favoris Article de flux Flux Police Couleur du texte Bibliothèque logicielle pour l'icône de la zone de notification Général Entrées cachées Longueur maximale du menu des entrées cachées Cacher Cacher tout Information Italique Langue Licence Couleur des liens Plus récents d'abord Nouvelle catégorie Suivant Non Pas de résultat. Normal Ok Plus anciens d'abord Opacité Ouvrir Ouvrir tout Merci de signaler ce bug sur  Position Précédent Quitter Exemple de texte Enregistrer sous Rechercher Préférences Afficher tout Trier Texte Le changement de bibliothèque logicielle prendra effet au prochain démarrage de l'application Le changement de langue prendra effet au prochain démarrage de l'application Titre Souligné Mise à jour Widget Widgets Oui Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme. Sinon, consultez http://www.gnu.org/licenses/. Visionneur de flux Arxiv {app_name} est déjà lancé, si ce n'est pas le cas, supprimez {pidfile}. {app_name} is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. {app_name} est un logiciel libre ; vous pouvez le diffuser et/ou le modifier suivant les termes de la Licence Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette licence, soit (à votre convenance) une version ultérieure. 