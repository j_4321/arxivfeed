#! /usr/bin/python3
# -*- coding:Utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018-2019 Juliette Monsel <j_4321@protonmail.com>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Config dialog
"""
import tkinter as tk
from tkinter import ttk

from PIL.ImageTk import PhotoImage

from arxivfeedlib.constants import CONFIG, FEEDS, TOOLKITS, \
    LANGUAGES, REV_LANGUAGES, RSS_CAT, APP_NAME, IM_COLOR
from arxivfeedlib.messagebox import showinfo
from .categories import CategoryManager
from .color import ColorFrame
from .opacity import OpacityFrame
from .font import FontFrame


class Config(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master, class_=APP_NAME)
        self.title(_("Settings"))
        self.grab_set()
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=1)
        self.minsize(470, 574)
        # self.resizable(False, True)

        self.changes = False, {}, False, False

        # entry validation
        self._validate = self.register(self._validate_menu_length)
        self._validate_title_size = self.register(lambda *args: self._validate_font_size(self.fonttitle_size, *args))
        self._validate_text_size = self.register(lambda *args: self._validate_font_size(self.font_size, *args))

        self._im_color = PhotoImage(master=self, file=IM_COLOR)

        self.notebook = ttk.Notebook(self)

        self.lang = tk.StringVar(self, LANGUAGES[CONFIG.get("General", "language")])
        self.gui = tk.StringVar(self, CONFIG.get("General", "trayicon").capitalize())

        self._init_general()
        self._init_widget()
        self.frame_cats = CategoryManager(self, master)
        self.notebook.add(self.frame_cats, text=_("Categories"))

        self.notebook.grid(sticky='ewsn', row=0, column=0, columnspan=2)
        ttk.Button(self, text=_('Ok'), command=self.ok).grid(row=1, column=0,
                                                             sticky='e', padx=4,
                                                             pady=10)
        ttk.Button(self, text=_('Cancel'), command=self.destroy).grid(row=1, column=1,
                                                                      sticky='w',
                                                                      padx=4,
                                                                      pady=10)

    def _init_general(self):
        frame_general = ttk.Frame(self)
        frame_general.columnconfigure(1, weight=1)
        self.notebook.add(frame_general, text=_("General"))
        # --- Language
        ttk.Label(frame_general, text=_("Language")).grid(row=0, column=0,
                                                          padx=8, pady=4, sticky="e")

        menu_lang = tk.Menu(frame_general)
        mb = ttk.Menubutton(frame_general, menu=menu_lang, width=9,
                            textvariable=self.lang)
        mb.grid(row=0, column=1, padx=8, pady=4, sticky="w")
        width = 0
        for lang in LANGUAGES:
            language = LANGUAGES[lang]
            width = max(width, len(language))
            menu_lang.add_radiobutton(label=language, value=language,
                                      variable=self.lang, command=self.translate)
        mb.configure(width=width)

        # --- gui toolkit
        ttk.Label(frame_general, wraplength=320,
                  text=_("GUI Toolkit for the system tray icon")).grid(row=1, column=0,
                                                                       padx=8, pady=4,
                                                                       sticky="ew")

        menu_gui = tk.Menu(frame_general)
        ttk.Menubutton(frame_general, menu=menu_gui, width=9,
                       textvariable=self.gui).grid(row=1, column=1,
                                                   padx=8, pady=4, sticky="w")
        for toolkit, b in TOOLKITS.items():
            if b:
                menu_gui.add_radiobutton(label=toolkit.capitalize(),
                                         value=toolkit.capitalize(),
                                         variable=self.gui,
                                         command=self.change_gui)
        # --- Update checks
        self.confirm_update = ttk.Checkbutton(frame_general,
                                              text=_("Check for updates on start-up"))
        self.confirm_update.grid(row=2, column=0, padx=8, pady=4, columnspan=2, sticky='w')
        if CONFIG.getboolean('General', 'check_update', fallback=True):
            self.confirm_update.state(('selected', '!alternate'))
        else:
            self.confirm_update.state(('!selected', '!alternate'))

        # --- Splash supported
        self.splash_support = ttk.Checkbutton(frame_general,
                                              text=_("Check this box if the widgets disappear when you click"))
        self.splash_support.grid(row=3, column=0, padx=8, pady=4, columnspan=2, sticky='w')
        if not CONFIG.getboolean('General', 'splash_supported', fallback=True):
            self.splash_support.state(('selected', '!alternate'))
        else:
            self.splash_support.state(('!selected', '!alternate'))

        # --- menu length
        ttk.Label(frame_general, wraplength=320,
                  text=_("Hidden entries menu maximum length")).grid(row=4, column=0,
                                                                     padx=8, pady=4,
                                                                     sticky="ew")

        self.menu_length = ttk.Entry(frame_general, width=4, validate='key',
                                     validatecommand=(self._validate, '%P'),
                                     justify='center')
        self.menu_length.insert(0, CONFIG.getint("General", "menu_length", fallback=30))
        self.menu_length.grid(row=4, column=1, padx=8, pady=4, sticky="w")

        ttk.Separator(frame_general, orient='horizontal').grid(row=5,
                                                               columnspan=2,
                                                               sticky='ew')

        # --- feeds
        self._cats = []
        frame_cats = ttk.Frame(frame_general)
        frame_cats.grid(row=6, columnspan=2, sticky='ew', padx=4, pady=8)
        ttk.Label(frame_cats, text=_('Active feeds:')).grid(row=0, column=0,
                                                            sticky='nw', padx=4)
        for i, cat in enumerate(RSS_CAT):
            c = ttk.Checkbutton(frame_cats, text=cat)
            self._cats.append(c)
            b = not FEEDS.getboolean(cat, 'active')
            c.state(('!' * b + 'selected', '!alternate'))
            c.grid(row=i // 2, column=i % 2 + 1, sticky='w')

    def _init_widget(self):
        frame_widget = ttk.Frame(self)
        self.notebook.add(frame_widget, text=_('Widget'))

        # --- font
        frame_font = ttk.Frame(frame_widget)
        self.title_font = FontFrame(frame_font, CONFIG.get("Widget", "font_title"), True)
        self.text_font = FontFrame(frame_font, CONFIG.get("Widget", "font"))
        frame_font.columnconfigure(1, weight=1)
        ttk.Label(frame_font,
                  text=_('Title')).grid(row=0, column=0, sticky='nw', padx=4, pady=4)
        self.title_font.grid(row=0, column=1)
        ttk.Separator(frame_font, orient='horizontal').grid(row=1, columnspan=2,
                                                            sticky='ew', padx=4,
                                                            pady=4)
        ttk.Label(frame_font,
                  text=_('Text')).grid(row=2, column=0, sticky='nw', padx=4, pady=4)
        self.text_font.grid(row=2, column=1)

        # --- opacity
        self.opacity_frame = OpacityFrame(frame_widget, CONFIG.get("Widget", "alpha"))

        # --- colors
        frame_color = ttk.Frame(frame_widget)
        frame_color.columnconfigure(1, weight=1)
        frame_color.columnconfigure(3, weight=1)
        self.color_bg = ColorFrame(frame_color,
                                   CONFIG.get("Widget", "background"),
                                   _('Background color'))
        self.color_fg = ColorFrame(frame_color,
                                   CONFIG.get("Widget", "foreground"),
                                   _('Foreground color'))
        self.color_feed_bg = ColorFrame(frame_color,
                                        CONFIG.get("Widget", "feed_background"),
                                        _('Background color'))
        self.color_feed_fg = ColorFrame(frame_color,
                                        CONFIG.get("Widget", "feed_foreground"),
                                        _('Foreground color'))
        self.color_link = ColorFrame(frame_color,
                                     CONFIG.get("Widget", "link_color"),
                                     _('Link color'))
        ttk.Label(frame_color,
                  text=_('General')).grid(row=0, column=0, sticky='w', padx=4, pady=2)
        self.color_bg.grid(row=0, column=1, sticky='e', padx=4, pady=2)
        self.color_fg.grid(row=1, column=1, sticky='e', padx=4, pady=2)

        ttk.Separator(frame_color, orient='horizontal').grid(row=2, columnspan=4,
                                                             sticky='ew', padx=4,
                                                             pady=4)
        ttk.Label(frame_color,
                  text=_('Feed entry')).grid(row=3, column=0, sticky='w', padx=4, pady=2)
        self.color_feed_bg.grid(row=3, column=1, sticky='e', padx=4, pady=2)
        self.color_feed_fg.grid(row=4, column=1, sticky='e', padx=4, pady=2)
        self.color_link.grid(row=5, column=1, sticky='e', padx=4, pady=2)

        # --- pack
        ttk.Label(frame_widget, text=_('Font'),
                  font='TkDefaultFont 9 bold', anchor='w').pack(padx=4, fill='x')
        frame_font.pack(fill='x', padx=14)
        ttk.Separator(frame_widget, orient='horizontal').pack(fill='x', pady=6)
        self.opacity_frame.pack(padx=(4, 10), fill='x')
        ttk.Separator(frame_widget, orient='horizontal').pack(fill='x', pady=6)
        ttk.Label(frame_widget, text=_('Colors'),
                  font='TkDefaultFont 9 bold', anchor='w').pack(padx=4, fill='x')
        frame_color.pack(fill='x', padx=14)

    def display_label(self, value):
        self.opacity_label.configure(text=" {val} %".format(val=int(float(value))))

    def translate(self):
        showinfo(_("Information"),
                 _("The language setting will take effect after restarting the application"),
                 parent=self)

    def change_gui(self):
        showinfo(_("Information"),
                 _("The GUI Toolkit setting will take effect after restarting the application"),
                 parent=self)

    @staticmethod
    def _config_size(variable, font):
        size = variable.get()
        if size:
            font.configure(size=size)

    @staticmethod
    def _validate_menu_length(P):
        """ Allow only to enter numbers"""
        return (P == "" or P.isdigit())

    def ok(self):
        old_splash_supp = CONFIG.get('General', 'splash_supported', fallback=True)
        splash_supp = str(not self.splash_support.instate(('selected',)))
        # --- general
        CONFIG.set("General", "language", REV_LANGUAGES[self.lang.get()])
        CONFIG.set("General", "trayicon", self.gui.get().lower())
        CONFIG.set('General', 'check_update', str(self.confirm_update.instate(('selected',))))
        CONFIG.set('General', 'splash_supported', splash_supp)
        old_menu_length = CONFIG.get('General', 'menu_length', fallback='30')
        menu_length = self.menu_length.get()
        if not menu_length:
            menu_length = old_menu_length
        if int(menu_length) < 1:
            menu_length = '1'
        CONFIG.set('General', 'menu_length', menu_length)

        # --- widget
        CONFIG.set("Widget", "alpha", "%i" % self.opacity_frame.get_opacity())

        font_title_dic = self.title_font.get_font()
        font_title_dic['underline'] = 'underline' if font_title_dic['underline'] else ''
        font_title_dic['family'] = font_title_dic['family'].replace(' ', '\ ')
        CONFIG.set("Widget", "font_title", "{family} {size} {weight} {slant} {underline}".format(**font_title_dic))
        font_text_dic = self.text_font.get_font()
        font_text_dic['family'] = font_text_dic['family'].replace(' ', '\ ')
        CONFIG.set("Widget", "font", "{family} {size}".format(**font_text_dic))
        CONFIG.set("Widget", "foreground", self.color_fg.get_color())
        CONFIG.set("Widget", "background", self.color_bg.get_color())
        CONFIG.set("Widget", "feed_foreground", self.color_feed_fg.get_color())
        CONFIG.set("Widget", "feed_background", self.color_feed_bg.get_color())
        CONFIG.set("Widget", "link_color", self.color_link.get_color())
        for c in self._cats:
            if 'selected' in c.state():
                FEEDS.set(c.cget('text'), 'active', "True")
            else:
                FEEDS.set(c.cget('text'), 'active', "False")

        name_changes, cat_change = self.frame_cats.save()
        self.changes = old_splash_supp != splash_supp, name_changes, cat_change, menu_length != old_menu_length
        self.destroy()
