#! /usr/bin/python3
# -*- coding:Utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018-2019 Juliette Monsel <j_4321@protonmail.com>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Category manager
"""
import tkinter as tk
from tkinter import ttk

from PIL.ImageTk import PhotoImage

from arxivfeedlib.constants import CATEGORIES, APP_NAME, RESERVED_NAMES, \
    IM_ADD, IM_DEL, save_categories, create_cat
from arxivfeedlib.messagebox import askokcancel
from arxivfeedlib.autoscrollbar import AutoScrollbar


class CategoryManager(ttk.Frame):
    """Category manager for the entries."""

    def __init__(self, master, app, **kwargs):
        """Create category manager."""
        ttk.Frame.__init__(self, master, padding=4, **kwargs)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        self.app = app

        self._im_add = PhotoImage(file=IM_ADD)
        self._im_del = PhotoImage(file=IM_DEL)

        style = ttk.Style(self)

        txt = tk.Text(self, width=1, height=1, relief='flat',
                      bg=style.lookup('TFrame', 'background'),
                      highlightthickness=0, padx=6, pady=6, cursor='arrow')
        scroll_x = AutoScrollbar(self, orient='horizontal', command=txt.xview)
        scroll_y = AutoScrollbar(self, orient='vertical', command=txt.yview)
        txt.configure(xscrollcommand=scroll_x.set, yscrollcommand=scroll_y.set)

        txt.grid(row=0, column=0, sticky='ewns')
        scroll_x.grid(row=1, column=0, sticky='ew')
        scroll_y.grid(row=0, column=1, sticky='ns')

        self.frame_cat = ttk.Frame(txt)
        txt.window_create('1.0', window=self.frame_cat)
        txt.configure(state='disabled')

        self.old_categories = list(CATEGORIES.sections())
        self.old_categories.sort()

        self.cats = {}
        for i, cat in enumerate(self.old_categories):
            label = ttk.Label(self.frame_cat, text=cat, anchor='e')
            label.grid(row=i, column=0, sticky="ew", padx=2)
            label.bind('<Double-Button-1>', self.change_name)
            b_del = ttk.Button(self.frame_cat, padding=0, image=self._im_del,
                               command=lambda c=cat: self.del_cat(c))
            b_del.grid(row=i, column=1, padx=4, pady=4, sticky='ns')
            self.cats[cat] = label, b_del

        ttk.Button(self, image=self._im_add,
                   command=self.add_cat).grid(row=2, column=0, sticky='w', pady=8)

    def change_name(self, event):
        """Change category name."""

        def ok(event):
            cats = [l.cget('text') for l, b in self.cats.values()]
            cat = name.get().strip()
            if cat and cat not in cats and cats not in RESERVED_NAMES:
                label.configure(text=cat)
            name.destroy()

        label = event.widget
        name = ttk.Entry(self, justify='center')
        name.insert(0, label.cget('text'))
        name.place(in_=label, relx=0, rely=0, anchor='nw', relwidth=1, relheight=1)
        name.bind('<FocusOut>', lambda e: name.destroy())
        name.bind('<Escape>', lambda e: name.destroy())
        name.bind('<Return>', ok)
        name.selection_range(0, 'end')
        name.focus_set()

    def del_cat(self, category):
        """Remove category."""
        label, b_del = self.cats[category]
        rep = askokcancel(_("Confirmation"),
                          _("Do you really want to delete the category {category}?").format(category=label.cget('text')))

        if rep:
            del self.cats[category]
            label.destroy()
            b_del.destroy()
            CATEGORIES.remove_section(category)
            self.app.remove_category(category)
            save_categories()

    def add_cat(self):
        """Add a category."""
        top = tk.Toplevel(self, class_=APP_NAME)
        top.transient(self)
        top.grab_set()
        top.resizable(False, False)
        top.title(_("New Category"))

        def valide(event=None):
            cats = [l.cget('text') for l, b in self.cats.values()]
            cat = name.get().strip()
            if cat and cat not in cats:
                c, i = self.frame_cat.grid_size()
                label = ttk.Label(self.frame_cat, text=cat, anchor='e')
                label.grid(row=i, column=0, sticky="ew", padx=2)
                label.bind('<Double-Button-1>', self.change_name)
                b_del = ttk.Button(self.frame_cat, padding=0, image=self._im_del,
                                   command=lambda c=cat: self.del_cat(c))
                b_del.grid(row=i, column=1, padx=4, pady=4, sticky='ns')
                self.cats[cat] = label, b_del
                top.destroy()

        name = ttk.Entry(top, justify="center")
        name.grid(row=0, column=0, columnspan=2, sticky="ew")
        name.bind("<Return>", valide)
        name.bind("<Escape>", lambda e: top.destroy())
        name.focus_set()
        ttk.Button(top, text="Ok",
                   command=valide).grid(row=1, column=0, sticky="nswe")
        ttk.Button(top, text=_("Cancel"),
                   command=top.destroy).grid(row=1, column=1, sticky="nswe")

    def save(self):
        name_changes = {}
        cat_change = self.old_categories != sorted(self.cats.keys())
        categories = CATEGORIES.sections()
        for cat, (label, b_del) in self.cats.items():
            new_name = label.cget('text')
            if cat in categories:
                if new_name != cat:
                    name_changes[new_name] = cat
                    create_cat(new_name, CATEGORIES.get(cat, 'geometry'),
                               CATEGORIES.get(cat, 'position'),
                               CATEGORIES.get(cat, 'visible'))
                    CATEGORIES.remove_section(cat)

            else:
                create_cat(new_name, visible='True')

        save_categories()
        return name_changes, cat_change
