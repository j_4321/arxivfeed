#! /usr/bin/python3
# -*- coding:Utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018-2019 Juliette Monsel <j_4321@protonmail.com>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Check for updates
"""
import re
from threading import Thread
from urllib import request, error
from html.parser import HTMLParser
from webbrowser import open as webOpen
from tkinter import Toplevel, PhotoImage
from tkinter.ttk import Label, Button, Frame, Checkbutton
import logging

from .constants import CONFIG, save_config, IM_QUESTION_DATA, APP_NAME
from . import __version__


class VersionParser(HTMLParser):
    def __init__(self, *args, **kwargs):
        HTMLParser.__init__(self, *args, **kwargs)
        self.versions = ['0.0.0']
        self._title = False

    def handle_starttag(self, tag, attrs):
        self._title = tag == "title"

    def handle_endtag(self, tag):
        self._title = False

    def handle_data(self, data):
        if self._title:
            res = re.match(r"^v[0-9]+\.[0-9]+\.[0-9]+", data)
            if res:
                self.versions.append(res.group()[1:])

    def feed(self, data):
        self.versions = ['0.0.0']
        HTMLParser.feed(self, data)
        return max(self.versions)


class UpdateChecker(Toplevel):

    version_parser = VersionParser()

    def __init__(self, master):
        Toplevel.__init__(self, master)
        self.title(_("Update"))
        self.withdraw()
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.protocol("WM_DELETE_WINDOW", self.quit)

        self.trial_nb = 0

        self._version = __version__

        self.img = PhotoImage(data=IM_QUESTION_DATA, master=self)

        frame = Frame(self)
        frame.grid(row=0, columnspan=2, sticky="ewsn")
        Label(frame, image=self.img).pack(side="left", padx=(10, 4), pady=(10, 4))
        Label(frame,
              text=_("A new version of {app_name} is available.\nDo you want to download it?").format(app_name=APP_NAME),
              font="TkDefaultFont 10 bold",
              wraplength=335).pack(side="left", padx=(4, 10), pady=(10, 4))

        self.b1 = Button(self, text=_("Yes"), command=self.download)
        self.b1.grid(row=1, column=0, padx=10, pady=10, sticky="e")
        Button(self, text=_("No"), command=self.quit).grid(row=1, column=1,
                                                           padx=10, pady=10,
                                                           sticky="w")
        self.ch = Checkbutton(self, text=_("Check for updates on startup."))
        if CONFIG.getboolean("General", "check_update"):
            self.ch.state(("selected", ))
        self.ch.grid(row=2, columnspan=2, sticky='w')
        self.update = None
        self._error = ''

        self.thread = Thread(target=self.update_available, daemon=True)
        self.thread.start()
        self.after(1000, self.check_update)

    def check_update(self):
        if self.update is None:
            self.after(1000, self.check_update)
        elif self.update:
            logging.info("%s %s is available", APP_NAME, self._version)
            self.deiconify()
            self.grab_set()
            self.lift()
            self.b1.focus_set()
        else:
            if self._error:
                logging.error("Update check failed: {err}".format(err=self._error))
            else:
                logging.info("%s is up-to-date", APP_NAME)
            self.destroy()

    def quit(self):
        CONFIG.set("General", "check_update", str("selected" in self.ch.state()))
        save_config()
        self.destroy()

    def download(self):
        webOpen("https://gitlab.com/j_4321/{app_name}/-/archive/v{version}/{app_name}-v{version}.tar.gz".format(app_name=APP_NAME,
                                                                                                                version=self._version))
        self.quit()

    def update_available(self):
        """
        Check for updates online.

        Return True if an update is available, False
        otherwise (and if there is no Internet connection).
        """
        if self.trial_nb < 5:
            self._error = ''
            try:
                with request.urlopen('https://gitlab.com/j_4321/{app_name}/tags?feed_token=qoBw5EouBX1zS_4Rm-sf&format=atom'.format(app_name=APP_NAME)) as page:
                    latest_version = self.version_parser.feed(page.read().decode())
                self.update = latest_version > __version__
                self._version = max(latest_version, __version__)
            except error.URLError as e:
                self._error = str(e)
                try:
                    if e.reason.errno == -2:
                        # no Internet connection
                        self.update = False
                    else:
                        self.trial_nb += 1
                        self.update_available()
                except AttributeError:
                    self.trial_nb += 1
                    self.update_available()
        else:
            self.update = False
