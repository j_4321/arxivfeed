#! /usr/bin/python3
# -*- coding:Utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018 Juliette Monsel <j_4321@protonmail.com>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Desktop widget displaying entries in category
"""
from tkinter.ttk import Entry

from .widget import Widget
from arxivfeedlib.constants import CATEGORIES, save_categories, RESERVED_NAMES


class CatWidget(Widget):

    def __init__(self, master, category):
        """
        Category widget

        name:
            category's name
        position:
            'normal', 'above', 'below'
        geometry:
            widget geometry 'width x height + x + y'
        visible:
            whether to show the widget
        """
        Widget.__init__(self, master, category, CATEGORIES, save_categories)
        self.label.bind('<Double-1>', self.edit_name)

    def edit_name(self, event):

        def ok(event):
            new_name = entry.get().strip()
            if new_name and new_name not in CATEGORIES.sections() and new_name not in RESERVED_NAMES:
                self.master.rename_category(self.name, new_name, True)
            entry.destroy()

        entry = Entry(self, justify='center')
        entry.insert(0, self.name)
        entry.selection_range(0, 'end')
        entry.place(in_=self.label, x=0, y=0, anchor='nw', relwidth=1, relheight=1)
        entry.bind('<Return>', ok)
        entry.bind('<Escape>', lambda e: entry.destroy())
        entry.bind('<FocusOut>', lambda e: entry.destroy())

    def rename(self, name):
        self.name = name
        self.label.configure(text=name)

    def entry_add(self, title, summary, authors, url, date=None, index=None):
        """Display entry."""
        if index is None:
            index = self.display.grid_size()[1] + 1
            if title in self.entries:
                tf, txt, cats, b = self.entries[title]
                try:
                    index = tf.grid_info()['row']
                except KeyError:
                    pass

        Widget.entry_add(self, title, summary, authors, url, date, index)
