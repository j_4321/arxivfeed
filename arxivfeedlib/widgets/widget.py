#! /usr/bin/python3
# -*- coding:Utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018-2019 Juliette Monsel <j_4321@protonmail.com>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Desktop widget displaying feed content
"""
import datetime
import pickle
from tkinter import Toplevel, BooleanVar, Menu, StringVar, Canvas
from tkinter.ttk import Label, Separator, Sizegrip, Frame, Button, Entry, Combobox
from webbrowser import open as webopen

from ewmh import ewmh
from PIL.ImageTk import PhotoImage

from arxivfeedlib.constants import CONFIG, APP_NAME, add_trace, EWMH, \
    PATH_FEED_DATA, IM_SEARCH, CATEGORIES
from arxivfeedlib.autoscrollbar import AutoScrollbar
from arxivfeedlib.toggledframe import ToggledFrame
from arxivfeedlib.feed_utils import FeedText
from arxivfeedlib.longmenu import LongMenu
from arxivfeedlib.messagebox import showinfo


class CatCombobox(Combobox):
    def __init__(self, title, *args, **kwargs):
        kwargs.setdefault('state', 'readonly')
        kwargs.setdefault('exportselection', False)
        kwargs.setdefault('style', 'widget.TCombobox')
        self.title = title
        Combobox.__init__(self, *args, **kwargs)
        self.tk.eval("""
proc ComboListKeyPressed {w key} {
        if {[string length $key] > 1 && [string tolower $key] != $key} {
                return
        }

        set cb [winfo parent [winfo toplevel $w]]
        set text [string map [list {[} {\[} {]} {\]}] $key]
        if {[string equal $text ""]} {
                return
        }

        set values [$cb cget -values]
        set x [lsearch -glob -nocase $values $text*]
        if {$x < 0} {
                return
        }

        set current [$w curselection]
        if {$current == $x && [string match -nocase $text* [lindex $values [expr {$x+1}]]]} {
                incr x
        }

        $w selection clear 0 end
        $w selection set $x
        $w activate $x
        $w see $x
}

set popdown [ttk::combobox::PopdownWindow %s]
bind $popdown.f.l <KeyPress> [list ComboListKeyPressed %%W %%K]
""" % (self))
        self.bind('<Button-4>', lambda e: 'break')
        self.bind('<Button-5>', lambda e: 'break')

    def update_values(self, values):
        self.configure(values=values)
        if self.get() not in values:
            self.set('')

    def config_popdown(self, background, foreground):
        self.tk.eval('set popdown [ttk::combobox::PopdownWindow %s]' % self)
        self.tk.eval('$popdown.f.l configure -background %s -foreground %s' % (background, foreground))


class Widget(Toplevel):

    def __init__(self, master, name, config, save_config):
        """
        Base class for Desktop widgets

        name:
            widget's name
        position:
            'normal', 'above', 'below'
        geometry:
            widget geometry 'width x height + x + y'
        visible:
            whether to show the widget
        """
        Toplevel.__init__(self, master)
        self.rowconfigure(2, weight=1)
        self.columnconfigure(0, weight=1)

        self.name = name
        self.config = config
        self.save_config = save_config

        self.protocol('WM_DELETE_WINDOW', self.quit)
        self.attributes('-type', 'splash')
        self.minsize(50, 50)

        # control main menu checkbutton
        self.variable = BooleanVar(self, self.config.getboolean(name, 'visible'))

        self._position = StringVar(self, self.config.get(name, 'position'))
        add_trace(self._position, 'write', self._position_trace)

        self._sort_order = StringVar(self, self.config.get(name, 'sort_order',
                                                           fallback='oldest'))
        add_trace(self._sort_order, 'write', self._order_trace)
        self._mindate = datetime.datetime(datetime.MINYEAR, 1, 1)  # for date ordering when entry has no date

        self.title('{app_name}.widget.{name}'.format(app_name=APP_NAME, name=name))
        self.withdraw()

        self.entries = {}
        try:
            with open(PATH_FEED_DATA.format(category=self.name), 'rb') as file:
                up = pickle.Unpickler(file)
                self._content = up.load()
        except Exception:
            self._content = {}

        self.x = None
        self.y = None

        self.categories = [""] + list(CATEGORIES.sections())
        self.categories.sort()

        # --- menu
        self._create_menu()

        # --- elements
        self._im_search = PhotoImage(file=IM_SEARCH, master=self)
        frame = Frame(self, style='widget.TFrame')
        Button(frame, style='widget.hide.TButton',
               command=self.withdraw).pack(side='left')
        self.label = Label(frame, text=name, style='widget.title.TLabel',
                           anchor='center')
        self.label.pack(side='left', fill='x', expand=True)
        frame.grid(row=0, columnspan=2, padx=4, pady=4, sticky='ew')
        sep = Separator(self, style='widget.Horizontal.TSeparator')
        sep.grid(row=1, columnspan=2, sticky='ew')
        self.canvas = Canvas(self, highlightthickness=0)
        self.canvas.grid(row=2, column=0, sticky='ewsn', padx=(2, 8), pady=(2, 4))
        scroll = AutoScrollbar(self, orient='vertical',
                               style='widget.Vertical.TScrollbar',
                               command=self.canvas.yview)
        scroll.grid(row=2, column=1, sticky='ns', pady=(2, 14))
        self.canvas.configure(yscrollcommand=scroll.set)
        self.display = Frame(self.canvas, style='widget.TFrame')
        self.canvas.create_window(0, 0, anchor='nw', window=self.display, tags=('display',))

        self.display.columnconfigure(0, weight=1)

        # --- style
        self.update_style()

        corner = Sizegrip(self, style="widget.TSizegrip")
        corner.place(relx=1, rely=1, anchor='se', bordermode='outside')

        geometry = self.config.get(name, 'geometry')
        if geometry:
            self.geometry(geometry)
        self.update_idletasks()

        if self.config.getboolean(name, 'visible'):
            self.deiconify()

        # --- search dialog
        self._search_res = []
        self._search_index = -1

        self._search_window = Toplevel(self, padx=4, pady=4)
        self._search_window.protocol('WM_DELETE_WINDOW', self._search_close)
        self._search_window.attributes('-topmost', True)
        self._search_window.withdraw()
        self._search_window.resizable(True, False)
        self._search_window.title(_('Search') + ' - ' + self.name)
        self._search_window.columnconfigure(0, weight=1)
        self._search_window.columnconfigure(1, weight=1)
        frame = Frame(self._search_window)
        self._search_entry = Entry(frame)
        Button(frame, image=self._im_search, width=1, command=self._search,
               padding=1).pack(side='right', padx=4, pady=4)
        self._search_entry.pack(side='left', fill='x', expand=True, padx=4, pady=4)
        self._search_entry.bind('<Return>', self._search)
        frame.grid(columnspan=2, sticky='ew')
        self._search_entry.focus_set()
        Button(self._search_window, text='← ' + _('Previous'), command=self._prev_res,
               padding=1).grid(row=1, column=0, sticky='ew', padx=4, pady=4)
        Button(self._search_window, text='→ ' + _('Next'), command=self._next_res,
               padding=1).grid(row=1, column=1, sticky='ew', padx=4, pady=4)

        # --- bindings
        self.bind('<3>', lambda e: self.menu.tk_popup(e.x_root, e.y_root))
        for widget in [self.label, self.canvas, sep]:
            widget.bind('<ButtonPress-1>', self._start_move)
            widget.bind('<ButtonRelease-1>', self._stop_move)
            widget.bind('<B1-Motion>', self._move)
        self.bind('<Map>', self._change_position)
        self.bind('<Configure>', self._on_configure)
        self.bind('<Control-f>', self.search)
        self.bind('<4>', lambda e: self._scroll(-1))
        self.bind('<5>', lambda e: self._scroll(1))
        self.bind('<ButtonRelease-1>', lambda e: e.widget.focus_force())

        # --- restore entries
        self['cursor'] = 'watch'
        self.update_idletasks()
        for title, val in list(self._content.items()):
            self.entry_add(title, val['summary'], val['authors'], val['url'],
                           val.get('date', None))
        self.sort()
        self.update_idletasks()
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))

        if not CONFIG.getboolean('General', 'splash_supported', fallback=True) and self.variable.get():
            Toplevel.withdraw(self)
            Toplevel.deiconify(self)
        self['cursor'] = 'arrow'

    def quit(self):
        self.destroy()

    def save(self):
        with open(PATH_FEED_DATA.format(category=self.name), 'wb') as file:
            p = pickle.Pickler(file)
            p.dump(self._content)

    def _create_menu(self):
        self.menu = Menu(self)
        self.menu_hidden = LongMenu(self.menu)
        menu_pos = Menu(self.menu)
        menu_pos.add_radiobutton(label=_('Normal'), value='normal',
                                 variable=self._position, command=self._change_position)
        menu_pos.add_radiobutton(label=_('Above'), value='above',
                                 variable=self._position, command=self._change_position)
        menu_pos.add_radiobutton(label=_('Below'), value='below',
                                 variable=self._position, command=self._change_position)
        menu_sort = Menu(self.menu)

        menu_sort.add_radiobutton(label='A-Z',
                                  variable=self._sort_order, value='A-Z',
                                  command=lambda: self._sort_by_name(reverse=False))
        menu_sort.add_radiobutton(label='Z-A',
                                  variable=self._sort_order, value='Z-A',
                                  command=lambda: self._sort_by_name(reverse=True))
        menu_sort.add_radiobutton(label=_('Oldest first'),
                                  variable=self._sort_order, value='oldest',
                                  command=lambda: self._sort_by_date(reverse=False))
        menu_sort.add_radiobutton(label=_('Most recent first'),
                                  variable=self._sort_order, value='latest',
                                  command=lambda: self._sort_by_date(reverse=True))
        self.menu.add_cascade(label=_('Position'), menu=menu_pos)
        self.menu.add_command(label=_('Hide'), command=self.withdraw)
        self.menu.add_command(label=_('Open all'), command=self.open_all)
        self.menu.add_command(label=_('Close all'), command=self.close_all)
        self.menu.add_cascade(label=_('Sort'), menu=menu_sort)
        self.menu.add_cascade(label=_('Hidden entries'), menu=self.menu_hidden,
                              state='disabled')
        self.menu.add_command(label=_('Search'), command=self.search)

    def update_menu_length(self):
        self.menu_hidden.length = CONFIG.getint('General', 'menu_length')

    def update_style(self):
        self.attributes('-alpha', CONFIG.getint('Widget', 'alpha') / 100)
        bg = CONFIG.get('Widget', 'background')
        # feed_bg = CONFIG.get('Widget', 'feed_background')
        # feed_fg = CONFIG.get('Widget', 'feed_foreground')
        self.configure(bg=bg)
        self.canvas.configure(background=bg)
        for tf, txt, cats, b in self.entries.values():
            txt.update_style()
            # cats.config_popdown(feed_bg, feed_fg)

    def update_position(self):
        if self._position.get() == 'normal':
            if CONFIG.getboolean('General', 'splash_supported', fallback=True):
                self.attributes('-type', 'splash')
            else:
                self.attributes('-type', 'toolbar')
        if self.variable.get():
            Toplevel.withdraw(self)
            Toplevel.deiconify(self)

    def update_cats(self):
        self.categories = [""] + list(CATEGORIES.sections())
        self.categories.sort()
        for title, (tf, txt, cats, b) in self.entries.items():
            cats.update_values(self.categories)

    def _position_trace(self, *args):
        self.config.set(self.name, 'position', self._position.get())
        self.save_config()

    def _save_geometry(self):
        self.config.set(self.name, 'geometry', self.geometry())
        self.save_config()

    # --- entry management
    def open_all(self):
        for tf, txt, cats, b in self.entries.values():
            tf.open()
        self.update_idletasks()
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))

    def close_all(self):
        for tf, txt, cats, b in self.entries.values():
            tf.close()
        self.update_idletasks()
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))

    def clear(self):
        for tf, txt, cats, b in self.entries.values():
            tf.destroy()
        self.entries.clear()
        self._content.clear()

    def entry_delete(self, title):
        tf, txt, cats, b = self.entries[title]
        tf.destroy()
        if not self._content[title]['visible']:
            self.menu_hidden.delete(title)
            if self.menu_hidden.index('end') == 3:
                self.menu.entryconfigure(_('Hidden entries'), state='disabled')
        del self.entries[title]
        del self._content[title]

    def entry_show(self, title):
        self._content[title]['visible'] = True
        tf, txt, cats, b = self.entries[title]
        tf.grid()
        self.menu_hidden.delete(title)
        if self.menu_hidden.index('end') == 1:
            self.menu.entryconfigure(_('Hidden entries'), state='disabled')
        self.save()
        # show entry if not visible
        row = tf.grid_info()['row']
        frac = row / self.display.grid_size()[1]
        self.canvas.yview_moveto(frac)

    def entry_hide(self, title):
        tf, txt, cats, b = self.entries[title]
        tf.label.state(['!selected'])
        tf.grid_remove()
        self._content[title]['visible'] = False
        self.menu_hidden.add_command(label=title,
                                     command=lambda: self.entry_show(title))
        self.menu.entryconfigure(_('Hidden entries'), state='normal')
        self.update_idletasks()
        try:
            ind = self._search_res.index(title)
        except ValueError:
            pass
        else:
            self._search_res.remove(title)
            if self._search_index >= ind:
                self._search_index -= 1
                if self._search_index + 1 == ind:
                    self._next_res()

        self.save()

    def entry_add(self, title, summary, authors, url, date=None, index=None):
        """Display entry."""
        self.master.entries[title] = summary, authors, url, date

        if index is None:
            index = len(self.entries)

        if title in self.entries:
            tf, txt, cats, b = self.entries[title]
        else:
            if title not in self._content:
                self._content[title] = {'visible': True}
            if title not in self.master.fav_vars:
                self.master.fav_vars[title] = BooleanVar(self.master)
                add_trace(self.master.fav_vars[title], 'write',
                          lambda *x: self.master.fav_trace(title))
            if title not in self.master.cat_vars:
                self.master.cat_vars[title] = StringVar(self.master)
                self.master.prev_cat[title] = ''
                add_trace(self.master.cat_vars[title], 'write',
                          lambda *x: self.master.category_trace(title))
            tf = ToggledFrame(self.display, text=title.encode().decode('latex'),
                              style='widget.TFrame',
                              fav_var=self.master.fav_vars[title],
                              closecmd=lambda: self.entry_hide(title))

            tf.interior.configure(style='widget.interior.TFrame')
            tf.interior.columnconfigure(0, weight=1)
            txt = FeedText(tf.interior)
            frame = Frame(tf.interior, style='widget.interior.TFrame')
            Label(frame, text=_('Category'), style='widget.interior.TLabel').pack(side='left')
            cats = CatCombobox(title, frame, values=self.categories,
                               textvariable=self.master.cat_vars[title])
            cats.pack(side='left', fill='x', expand=True, padx=(2, 6))
            b = Button(frame, text='Open', style='widget.TButton')
            b.pack(side='right')
            txt.grid(row=0, column=0, padx=4, pady=4, sticky='ew')
            frame.grid(row=1, column=0, pady=4, padx=6, sticky='ew')
            self.entries[title] = (tf, txt, cats, b)

        self._content[title].update({'summary': summary, 'authors': authors,
                                     "url": url, 'date': date})

        tf.grid(row=index, sticky='ew', pady=2, padx=(8, 4))
        if not self._content[title]['visible']:
            tf.grid_remove()
            if not self.menu_hidden.in_menu(title):
                self.menu_hidden.add_command(label=title,
                                             command=lambda: self.entry_show(title))
                self.menu.entryconfigure(_('Hidden entries'), state='normal')
        txt.display_feed(authors, summary, date)
        b.configure(command=lambda: webopen(url))

    def see(self, title):
        h = self.canvas.bbox('all')[-1]
        y = self.entries[title][0].winfo_y()
        frac = y / h
        self.canvas.yview_moveto(frac)

    # --- search
    def _unselect(self):
        try:
            self.entries[self._search_res[self._search_index]][0].label.state(['!selected'])
        except Exception:
            pass

    def _search_clear(self):
        self._unselect()
        self._search_res.clear()
        self._search_index = -1

    def _next_res(self):
        if not self._search_res:
            showinfo(_("Search"), _("No result."), self._search_window)
            return
        self._unselect()
        self._search_index += 1
        self._search_index %= len(self._search_res)
        title = self._search_res[self._search_index]
        self.entries[title][0].label.state(['selected'])
        self.see(title)

    def _prev_res(self):
        if not self._search_res:
            showinfo(_("Search"), _("No result."), self._search_window)
            return
        self._unselect()
        self._search_index -= 1
        self._search_index %= len(self._search_res)
        title = self._search_res[self._search_index]
        self.entries[title][0].label.state(['selected'])
        self.see(title)

    def _search_close(self):
        self._search_window.withdraw()
        self._search_entry.delete(0, 'end')
        self._search_clear()

    def _search(self, event=None):
        self._search_clear()
        text = self._search_entry.get().lower()

        if not text:
            return
        for title in self._content:
            if self._content[title]['visible'] and text in title.lower():
                self._search_res.append(title)
        self._next_res()

    def search(self, event=None):
        if not self._search_window.winfo_ismapped():
            self._search_window.geometry('+%i+%i' % self.winfo_pointerxy())
            self._search_window.deiconify()
        self._search_window.attributes('-topmost', True)
        self._search_entry.focus_set()

    # --- window position and geometry
    def withdraw(self):
        Toplevel.withdraw(self)
        self.variable.set(False)

    def deiconify(self):
        Toplevel.deiconify(self)
        self.variable.set(True)

    def _order_trace(self, *args):
        self.config.set(self.name, 'sort_order', self._sort_order.get())
        self.save_config()

    def _date_sort_key(self, title):
        date = self._content[title]['date']
        return self._mindate if date is None else date

    def _sort_by_date(self, reverse):
        titles = sorted(self.entries.keys(), reverse=reverse,
                        key=self._date_sort_key)
        for i, title in enumerate(titles):
            tf, txt, cats, b = self.entries[title]
            tf.grid_configure(row=i)
            if not self._content[title]['visible']:
                tf.grid_remove()

    def _sort_by_name(self, reverse):
        titles = sorted(self.entries.keys(), reverse=reverse)
        for i, title in enumerate(titles):
            tf, txt, cats, b = self.entries[title]
            tf.grid_configure(row=i)
            if not self._content[title]['visible']:
                tf.grid_remove()

    def sort(self):
        order = self._sort_order.get()
        if order == 'A-Z':
            self._sort_by_name(False)
        elif order == 'Z-A':
            self._sort_by_name(True)
        elif order == 'oldest':
            self._sort_by_date(False)
        else:
            self._sort_by_date(True)

    def _scroll(self, delta):
        self.canvas.yview_scroll(delta, 'units')

    def _change_position(self, event=None):
        '''Make widget sticky and set its position with respects to the other windows.'''
        pos = self._position.get()
        splash_supp = CONFIG.getboolean('General', 'splash_supported', fallback=True)
        try:
            for w in EWMH.getClientList():
                if w.get_wm_name() == self.title():
                    EWMH.setWmState(w, 1, '_NET_WM_STATE_STICKY')
                    EWMH.setWmState(w, 1, '_NET_WM_STATE_SKIP_TASKBAR')
                    EWMH.setWmState(w, 1, '_NET_WM_STATE_SKIP_PAGER')
                    if pos == 'above':
                        self.attributes('-type', 'dock')
                        EWMH.setWmState(w, 1, '_NET_WM_STATE_ABOVE')
                        EWMH.setWmState(w, 0, '_NET_WM_STATE_BELOW')
                    elif pos == 'below':
                        self.attributes('-type', 'desktop')
                        EWMH.setWmState(w, 0, '_NET_WM_STATE_ABOVE')
                        EWMH.setWmState(w, 1, '_NET_WM_STATE_BELOW')
                    else:
                        if splash_supp:
                            self.attributes('-type', 'splash')
                        else:
                            self.attributes('-type', 'toolbar')
                        EWMH.setWmState(w, 0, '_NET_WM_STATE_BELOW')
                        EWMH.setWmState(w, 0, '_NET_WM_STATE_ABOVE')
            EWMH.display.flush()
            if event is None and not splash_supp:
                Toplevel.withdraw(self)
                Toplevel.deiconify(self)
        except ewmh.display.error.BadWindow:
            pass

    def _on_configure(self, event):
        if event.widget is self:
            geometry = self.geometry()
            if geometry != '1x1+0+0':
                self._save_geometry()
        elif event.widget in [self.canvas, self.display]:
            self.canvas.configure(scrollregion=self.canvas.bbox('all'))
            self.canvas.itemconfigure('display', width=self.canvas.winfo_width() - 4)

    def _start_move(self, event):
        self.x = event.x
        self.y = event.y
        self.configure(cursor='fleur')
        self.display.configure(cursor='fleur')

    def _stop_move(self, event):
        self.x = None
        self.y = None
        self.configure(cursor='arrow')
        self.display.configure(cursor='arrow')

    def _move(self, event):
        if self.x is not None and self.y is not None:
            deltax = event.x - self.x
            deltay = event.y - self.y
            x = self.winfo_x() + deltax
            y = self.winfo_y() + deltay
            self.geometry("+%s+%s" % (x, y))
