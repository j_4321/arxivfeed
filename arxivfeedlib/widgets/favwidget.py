#! /usr/bin/python3
# -*- coding:Utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018 Juliette Monsel <j_4321@protonmail.com>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Desktop widget displaying fav entries
"""
from .widget import Widget
from arxivfeedlib.constants import FEEDS, save_feeds


class FavWidget(Widget):

    def __init__(self, master):
        """
        Favorites widget

        name:
            widget's name
        position:
            'normal', 'above', 'below'
        geometry:
            widget geometry 'width x height + x + y'
        visible:
            whether to show the widget
        """
        Widget.__init__(self, master, 'Favorites', FEEDS, save_feeds)
        self.label.configure(text=_('Favorites'))
        self._search_window.title(_('Search') + ' - ' + _('Favorites'))

    def entry_add(self, title, summary, authors, url, date=None, index=None):
        """Display entry."""
        if index is None:
            index = self.display.grid_size()[1] + 1
            if title in self.entries:
                tf, txt, cats, b = self.entries[title]
                try:
                    index = tf.grid_info()['row']
                except KeyError:
                    pass

        Widget.entry_add(self, title, summary, authors, url, date, index)
