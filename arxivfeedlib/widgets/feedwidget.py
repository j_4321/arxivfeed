#! /usr/bin/python3
# -*- coding:Utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018 Juliette Monsel <j_4321@protonmail.com>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Desktop widget displaying feed content
"""
import logging
from threading import Thread
from datetime import datetime
from time import mktime

import feedparser

from arxivfeedlib.constants import FEEDS, save_feeds, RSS_URL
from arxivfeedlib.feed_utils import SummaryParser, AuthorParser
from arxivfeedlib.messagebox import showerror
from .widget import Widget


class FeedWidget(Widget):

    author_parser = AuthorParser()
    summary_parser = SummaryParser()

    def __init__(self, master, name):
        """
        Feed widgets

        name:
            widget's name
        position:
            'normal', 'above', 'below'
        geometry:
            widget geometry 'width x height + x + y'
        visible:
            whether to show the widget
        """
        Widget.__init__(self, master, name, FEEDS, save_feeds)
        self._after_id = ''
        self._data = {}

    def quit(self):
        try:
            self.after_cancel(self._after_id)
        except ValueError:
            pass
        self.destroy()

    def _create_menu(self):
        Widget._create_menu(self)
        self.menu.insert_command(4, label=_('Deactivate'), command=self.deactivate)
        self.menu.insert_command(5, label=_('Update'), command=self.update_feed)

    def deactivate(self):
        self.master.remove_widget(self.name)
        FEEDS.set(self.name, 'active', 'False')
        save_feeds()

    def entry_add(self, title, summary, authors, url, date=None, index=None):
        """Display entry."""
        Widget.entry_add(self, title, summary, authors, url, date, index)
        self.master.update_fav(title, summary, authors, url, date)
        self.master.update_category(title, summary, authors, url, date)

    def update_feed(self):
        try:
            self.after_cancel(self._after_id)
        except ValueError:
            pass
        logging.info('Updating {} ...'.format(self.name))
        thread = Thread(target=self._update_feed, daemon=True)
        thread.start()
        self._after_id = self.after(1000, self._check_update, thread)

    def _update_feed(self):
        url = RSS_URL.format(category=self.name)
        res = feedparser.parse(url)

        self._data.clear()

        if not res['entries']:
            # there was an error (e.g. non internet connection)
            err = res.get('bozo_exception', '')
            if err:
                logging.error("Update of %s failed: %s", self.name, err)
                msg = "{}: {}".format(type(err), " ".join([str(a) for a in err.args]))
                showerror(_("Error"), msg)
            return

        date = datetime.fromtimestamp(mktime(res['feed']['updated_parsed']))
        for entry in res['entries']:
            title = entry['title'].split('.')[0].strip()
            auths = entry['authors'][0]['name'].split(', ')
            authors = [self.author_parser.feed(a) for a in auths]
            summary = self.summary_parser.feed(entry['summary'])
            link = entry['link']
            self._data[title] = summary, authors, link, date

    def _check_update(self, thread):
        if thread.is_alive():
            self._after_id = self.after(1000, self._check_update, thread)
        elif not self._data:
            return
        else:
            self['cursor'] = 'watch'
            self.update_idletasks()
            j = 0
            for title, props in list(self._content.items()):
                if title not in self._data:
                    if not props['visible']:
                        self.entry_delete(title)
                    else:
                        tf, txt, cats, b = self.entries[title]
                        tf.grid(row=j, sticky='ew', pady=2, padx=(8, 4))
                        j += 1

            for i, (title, (summary, authors, url, date)) in enumerate(self._data.items(), j):
                self.entry_add(title, summary, authors, url, date=date, index=i)
            self.sort()
            logging.info('Updated {}'.format(self.name))
            self.save()
            self.master.save_favs()
            self._after_id = self.after(3600000, self.update_feed)
            self['cursor'] = 'arrow'
