arxivfeed - arXiv feed viewer
=============================

Linux application to display arXiv feeds by category in desktop widgets.
The application is managed from the system tray. Categories can be activated
from the setting dialog. Each widget displays the titles of the articles from the
feed and items can be exanded to show the abstract with a limited latex rendering
of mathematical formulas.


Install
-------

..  - Archlinux

..      arxivfeed is available in `AUR <https://aur.archlinux.org/packages/arxivfeed>`_.

..  - Ubuntu

..      arxivfeed is available in the PPA `ppa:j-4321-i/ppa <https://launchpad.net/~j-4321-i/+archive/ubuntu/ppa>`_.

..      ::

..          $ sudo add-apt-repository ppa:j-4321-i/ppa
..          $ sudo apt-get update
..          $ sudo apt-get install arxivfeed

- Source code

    First, install the missing python librairies among:

     - Tkinter (Python wrapper for Tk)
     - feedparser https://pypi.python.org/pypi/feedparser
     - ewmh https://pypi.python.org/pypi/ewmh
     - Pillow https://pypi.python.org/pypi/Pillow
     - latexcodec https://pypi.python.org/pypi/latexcodec

    You also need to have at least one of the following GUI toolkits for the system tray icon:

     - Tktray https://code.google.com/archive/p/tktray/downloads
     - PyGTK http://www.pygtk.org/downloads.html
     - PyQt5, PyQt4 or PySide

    Then install the application:

    ::

        $ sudo python3 setup.py install

    You can now launch it from *Menu > Network > arxivfeed*. You can also launch
    it from the command line with ``arxivfeed``.

    Or just launch it from the command line:

    ::

        $ python3 arxivfeed

    Then click on the system tray icon and go to *Settings* to activate the 
    arXiv feeds you want to follow.
    

Troubleshooting
---------------

Several gui toolkits are available to display the system tray icon, so if the
icon does not behave properly, try to change toolkit, they are not all fully
compatible with every desktop environment.

If the widgets disappear when you click on them, open the setting dialog
from the menu and check the box 'Check this box if the widgets disappear
when you click'.

If you encounter bugs or if you have suggestions, please open an issue
on `Gitlab <https://gitlab.com/j_4321/arxivfeed/issues>`_.

