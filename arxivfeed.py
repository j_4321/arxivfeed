#! /usr/bin/python3
# -*- coding: utf-8 -*-
"""
arxivfeed - arXiv feed viewer
Copyright 2018 Juliette Monsel <j_4321@protonmail.com>

arxivfeed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

arxivfeed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


main
"""

from arxivfeedlib.messagebox import showerror
from arxivfeedlib.app import App
from arxivfeedlib.constants import PIDFILE, APP_NAME
import logging
import traceback
import sys
import os
from tkinter import Tk
from tkinter.ttk import Style

# check whether arxivfeed is running
pid = str(os.getpid())

if os.path.isfile(PIDFILE):
    with open(PIDFILE) as fich:
        old_pid = fich.read().strip()
    if os.path.exists("/proc/%s" % old_pid):
        with open("/proc/%s/cmdline" % old_pid) as file:
            cmdline = file.read()
        if 'arxivfeed' in cmdline:
            # arxivfeed is already runnning
            root = Tk()
            root.withdraw()
            style = Style(root)
            style.theme_use("clam")
            bg = style.lookup('TFrame', 'background', default='#ececec')
            root.option_add('*Toplevel.background', bg)
            logging.error("%s is already running", APP_NAME)
            showerror(_("Error"),
                      _("{app_name} is already running, if not delete {pidfile}.").format(app_name=APP_NAME,
                                                                                          pidfile=PIDFILE))
            sys.exit()
        else:
            # it is an old pid file
            os.remove(PIDFILE)
    else:
        # it is an old pid file
        os.remove(PIDFILE)

open(PIDFILE, 'w').write(pid)

try:
    app = App()
    app.mainloop()
except Exception as e:
    logging.exception(str(type(e)))
    showerror(_("Error"), str(type(e)), traceback.format_exc(), True)
finally:
    try:
        os.unlink(PIDFILE)
    except FileNotFoundError:
        # PIDFILE might have been deleted
        pass
    logging.shutdown()
